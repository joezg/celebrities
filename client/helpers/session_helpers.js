setGameState = function(_id, fieldName, value){
    Session.set(getFieldName(_id, fieldName), value);
}

getGameState = function(_id, fieldName){
    return Session.get(getFieldName(_id, fieldName));
}

var getFieldName = (_id, fieldName) => `game_${_id}_${fieldName}`;