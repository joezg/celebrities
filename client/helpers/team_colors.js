let possibleColors = [
    'red',
    'orange',
    'yellow',
    'olive',
    'green',
    'teal',
    'blue',
    'violet',
    'purple',
    'pink',
    'brown',
    'grey',
    'black'
]

setTeamColor = function(teamName, color){
    let index = possibleColors.indexOf(color);
    if (index === -1)
        throwError(`Color ${color} is not available!`);

    possibleColors.splice(index, 1);

    Session.set(getColorProperty(teamName), color);
}

getTeamColor = function(teamName){
    let color = Session.get(getColorProperty(teamName));

    if (!color){
        setTeamColor(teamName, getRandomColor());
    }

    return Session.get(getColorProperty(teamName));
}

let getRandomColor = function(){
    return Random.choice(possibleColors);
}

let getColorProperty = function (teamName){
    return teamName+'_color';
}