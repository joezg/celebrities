Template.dndList.created = function() {
    // Some of the code is copied from https://github.com/RubaXa/Sortable/blob/master/meteor/reactivize.js
    // "this" is a template instance that can store properties of our choice - http://docs.meteor.com/#/full/template_inst

    if (this.setupDone) return;  // paranoid: only run setup once

    // this.data is the data context - http://docs.meteor.com/#/full/template_data
    // normalize all options into templateInstance.options, and remove them from .data
    this.options = this.data.options || {};
    Object.keys(this.data).forEach(function(key) {
        if (key === 'options' || key === 'items') return;
        this.options[key] = this.data[key];
        delete this.data[key];
    }.bind(this));

    // We can get the collection via the .collection property of the cursor, but changes made that way
    // will NOT be sent to the server - https://github.com/meteor/meteor/issues/3271#issuecomment-66656257
    // Thus we need to use dburles:mongo-collection-instances to get a *real* collection
    if (this.data.items && this.data.items.collection) {
        this.collectionName = this.data.items.collection.name;
        this.collection = Mongo.Collection.get(this.collectionName);
    }

    this.setupDone = true;
};

Template.dndList.rendered = function() {
    // just compute the `data` context for each event
    // this ensures that each event will have data property on event argument
    ['onStart', 'onEnd', 'onSort', 'onFilter', 'onRemove', 'onAdd', 'onUpdate'].forEach(function(eventHandler) {
        if (this.options[eventHandler]) {
            var userEventHandler = this.options[eventHandler];
            this.options[eventHandler] = function(event) {
                var itemEl = event.item;  // dragged HTMLElement
                event.elementData = Blaze.getData(itemEl);

                if (event.from)
                    event.fromData = Blaze.getData(event.from);

                event.template = this;

                userEventHandler(event);
            }.bind(this);
        }
    }.bind(this));

    this.sortable = Sortable.create(this.firstNode.parentElement, this.options);
};

Template.sortable.destroyed = function () {
    if(this.sortable) this.sortable.destroy();
};

//example sortable callbacks
//let callbacks = {
//    onStart(event){
//        console.log('***** onStart');
//        console.dir(event.elementData);
//        console.dir(event.oldIndex);
//    },
//    onEnd(event){
//        console.log('***** onEnd');
//        console.dir(event.elementData);
//        console.dir(event.oldIndex);
//        console.dir(event.newIndex);
//    },
//    onAdd(event){
//        console.log('***** onAdd');
//        console.dir(event.fromData);
//        console.dir(event.elementData);
//        console.dir(event.oldIndex);
//        console.dir(event.newIndex);
//    },
//    onUpdate(event){ //only sort happened in one list
//        console.log('***** onUpdate');
//        console.dir(event.elementData);
//        console.dir(event.oldIndex);
//        console.dir(event.newIndex);
//    },
//    onSort(event){ //update, remove, add
//        console.log('***** onSort');
//        console.dir(event.elementData);
//        console.dir(event.oldIndex);
//        console.dir(event.newIndex);
//    },
//    onRemove(event){
//        console.log('***** onRemove');
//        console.dir(event.elementData);
//        console.dir(event.oldIndex);
//        console.dir(event.newIndex);
//    },
//    onFilter(event){
//        console.log('***** onFilter');
//        console.dir(event.elementData);
//        console.dir(event.oldIndex);
//        console.dir(event.newIndex);
//    }
//}