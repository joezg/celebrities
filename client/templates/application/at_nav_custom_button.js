// 'inherites' helpers from AccountsTemplates and adds additional features
let helpers = AccountsTemplates.atNavButtonHelpers;
helpers.userName = function(){
    return getUserName(Meteor.user());
}

Template.atNavCustomButton.helpers(AccountsTemplates.atNavButtonHelpers);

// Simply 'inherites' events from AccountsTemplates
Template.atNavCustomButton.events(AccountsTemplates.atNavButtonEvents);
