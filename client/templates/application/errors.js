Template.errors.helpers({
    errors: function() {
        return Errors.find();
    }
});

Template.error.events({
    'click .close.icon': function(event){
        $(event.target)
            .closest('.message')
            .transition('fade');
    }
})