/* global throwError */
Template.gameMenu.events({
    'submit #start-game': function(event){
        event.preventDefault();

        //do not enter empty name
        if (!event.target.location.value)
            return;

        let game = {
            location: event.target.location.value
        }

        let instance = Template.instance();

        Meteor.call('gameInsert', game, function(error, result) {
            // display the error to the user and abort
            if (error)
                return throwError(error);

            instance.newLocation.set(null);
            event.target.location.value = '';
            Router.go('gamePage', {_id: result._id});
        });
    },
    'submit #join-game': function(event){
        event.preventDefault();
        let instance = Template.instance();
        Meteor.call('gameJoinByCode', event.target.gameCode.value, function(error, result) {
            // display the error to the user and abort
            if (error)
                return throwError(error);

            Router.go('gamePage', {_id: result._id});
            instance.gameCode.set(null);
            event.target.gameCode.value = '';
        });
    },
    'input input[name=location]'(event){
        Template.instance().newLocation.set(event.target.value);
    },
    'input input[name=gameCode]'(event){
        Template.instance().gameCode.set(event.target.value);
    }
});

Template.gameMenu.helpers({
    canStartAGame(){
        return !!Template.instance().newLocation.get();
    },
    canJoinGame(){
        return !!Template.instance().gameCode.get();
    }
})

Template.gameMenu.onCreated(function(){
    let instance = this;
    instance.newLocation = new ReactiveVar(null);
    instance.gameCode = new ReactiveVar(null);
});
