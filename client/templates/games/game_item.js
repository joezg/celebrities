Template.gameItem.helpers({
    dateFromNow: function(){
        return moment(this.date).fromNow();
    }
});
