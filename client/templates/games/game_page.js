Template.gamePage.helpers({
    currentStateTemplate(){
        return GameState.findOne(Template.instance()._id).state;
    },
    gameExists(){
        return !!Games.findOne(Template.instance()._id);
    },
    game() {
        return Games.findOne(Template.instance()._id);
    }
});

Template.gamePage.onCreated(function(){
    let instance = this;

    instance._id = Router.current().params._id;
    instance.autorun(()=>{
        instance.subscribe('gameState', instance._id);
    });
})