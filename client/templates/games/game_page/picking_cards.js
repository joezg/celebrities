Template.pickingCards.helpers({
    players(){
        return GameTeamsPlayers.find({}, {sort: {teamName:1}});
    },
    teamColor(teamName){
        return getTeamColor(teamName);
    },
    numberOfCards(){
        let parentData = Template.parentData(1);
        return parentData.preferences.numberOfCardsPerPlayer;
    },
    pickedCards(playerId){
        return GameCards.find({playerIds: playerId}).count();
    },
    currentPlayer(){
        return GameTeamsPlayers.findOne({
            userPickingCards: Meteor.userId(),
            isPickingCardsInProgress: true
        })
    },
    canBeDone(){
        return isEveryUserDonePickingCards(this._id);
    }
});

Template.pickingCards.events({
    'click #startPicking'(event){
        Meteor.call('startPickingCards', this._id, (error)=> {
            if (error)
                throwError(error);
        })

    },
    'click #done'(event){
        Meteor.call('donePickingCards', this._id, (error)=> {
            if (error)
                throwError(error);
        })
    }
});

Template.pickingCards.onCreated(function(){
    let instance = this;

    instance.autorun(()=>{
        instance.subscribe('gameTeamsPlayers',instance.data._id );
        instance.subscribe('gameCardsConcealed',instance.data._id );
        instance.subscribe('gameState', instance.data._id );
    })
});