Template.usersSelectingPlayerOrder.helpers({
    teamColor(teamName){
        return getTeamColor(teamName);
    },
    orderCandidates(){
        return getOrderCandidates(this._id);
    }
});

Template.usersSelectingPlayerOrder.events({
    'click .player'(){
        Meteor.call('pickNextPlayer', this._id, (error) => {
            if (error)
                throwError(error);
        })
    },
    'click #random'(){
        let candidates = getOrderCandidates(this._id);

        if (!Array.isArray(candidates)){
            candidates = candidates.fetch();
        }

        let nextPlayer = Random.choice(candidates);

        Meteor.call('pickNextPlayer', nextPlayer._id, (error) => {
            if (error)
                throwError(error);
        })
    }
});

Template.usersSelectingPlayerOrder.onCreated(function () {
    let instance = this;

    instance.autorun(()=> {
        instance.subscribe('gameTeamsPlayers', instance.data._id);
    })
});