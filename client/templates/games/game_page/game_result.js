Template.gameResult.helpers({
    results(){
        let cards = GameCards.find({gameId: this._id});

        let scores = {};

        //current scores which cannot be calculated because it's not written in db, so it is passed as argument from roundReview template
        if (this.score && this.team && this.phase){
            scores[this.team] = createTeamScore(this.team);

            let phase = this.phase === 1 ? 'first' : this.phase === 2 ? 'second' : 'third';

            scores[this.team][phase+'PhaseScore'] = this.score;
            scores[this.team]['totalScore'] = this.score;
        }

        return _.chain(cards.fetch()).reduce(function(memo, card){
            ['first', 'second', 'third'].forEach( (phase) => {
                let team = card[phase+'PhaseWonByTeam'];

                if (team){
                    if (!memo[team]){
                        memo[team] = createTeamScore(team);
                    }

                    memo[team].totalScore++;
                    memo[team][phase+'PhaseScore']++;
                }

            });

            return memo;
        }, scores).toArray().value();
    },
    teamColor(teamName){
        return getTeamColor(teamName);
    }
});

let createTeamScore = function(team){
    return {
        team: team,
        firstPhaseScore: 0,
        secondPhaseScore: 0,
        thirdPhaseScore: 0,
        totalScore: 0
    }
}

Template.gameResult.onCreated(function(){
    let instance = this;

    instance.autorun(()=>{
        instance.subscribe('gameScore',instance.data._id );
    });

})