Template.gameReview.helpers({
    cards(){
        return GameCards.find({gameId: this._id}, {
            sort: {
                firstPhaseWonByPlayerId: 1,
                secondPhaseWonByPlayerId: 1,
                thirdPhaseWonByPlayerId: 1
            }
        });
    },
    playerNameById(id){
        return GameTeamsPlayers.findOne(id).playerName;
    },
    playerName(idField){
        return GameTeamsPlayers.findOne(this[idField]).playerName;
    },
    playerTeamColor(){
        let teamName =   GameTeamsPlayers.findOne(this.toString()).teamName;
        return getTeamColor(teamName);
    },
    teamColor(teamName){
        return getTeamColor(teamName);
    },
    teamThatWonColor(phaseNumber){
        let teamName = this[phaseNumber+'PhaseWonByTeam']
        return getTeamColor(teamName);
    },
    teamThatWonName(phaseNumber){
        return this[phaseNumber+'PhaseWonByTeam']
    }
});

Template.gameReview.events({
    "click #nextPhase"(){
        Router.go("/");
    }
})

Template.gameReview.onCreated(function(){
    let instance = this;

    instance.autorun(()=>{
        instance.subscribe('gameCardsPhaseReview',instance.data._id );
        instance.subscribe('gameTeamsPlayers',instance.data._id );
    })

    //phaseNumber
    let gs = GameState.findOne(instance.data._id);

    switch (gs.phase){
        case 1:
            instance.data.phaseNumber = 'first';
            break;
        case 2:
            instance.data.phaseNumber = 'second';
            break;
        case 3:
            instance.data.phaseNumber = 'third';
            break;
    }

});