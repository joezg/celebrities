Template.team.helpers({
    teamName(){
        return this.isUnsortedPlayersTeam ? UNSORTED_PLAYERS_TEAM_NAME : this.name;
    },
    players(){
        let selector = {
            teamName: this.name,
            $or: [
                { pendingAddToNewTeam:false },
                { usersAddingToNewTeam: { $ne: Meteor.userId() } }
            ]
        };
        return GameTeamsPlayers.find(selector, this.isUnsortedPlayersTeam ? {} : { sort: { order:1 } })
    },
    teamSortableOptions(){
        let options = {
            group: {
                name: 'players',
                pull: true,
                put: !this.isUnsortedPlayersTeam
            },
            filter: '.remove.player.button',
            sort: !this.isUnsortedPlayersTeam
        }

        if (!this.isUnsortedPlayersTeam){
            options.selector = {teamName: this.name};
            options.sortField = 'order';
            options.onAdd = function(event){
                let $set = {
                    $set: event.template.options.selector
                }
                $set.$set.pendingAddToNewTeam = false;
                $set.$set.usersAddingToNewTeam = [];

                let selector = event.elementData._id;

                GameTeamsPlayers.update(selector, $set);
            };
            options.onSort = function(event){

            };
        }

        return options;
    }
})

const UNSORTED_PLAYERS_TEAM_NAME = 'Unsorted players';