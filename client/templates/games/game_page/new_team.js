Template.newTeam.helpers({
    players(){
        let selector = Template.instance().selector;
        return GameTeamsPlayers.find(selector);
    },
    teamSortableOptions(){
        let templateInstance = Template.instance();

        let options = {
            group: {
                name: 'players',
                pull: true,
                put: true
            },
            sort: false,
            filter: '.remove.player.button',
            onAdd(event){
                GameTeamsPlayers.update({_id: event.elementData._id}, {
                    $set: {
                        pendingAddToNewTeam: true
                    },
                    $push:{
                        usersAddingToNewTeam: Meteor.userId()
                    }
                })
            }
        }

        return options;
    },
    addingNewTeam(){
        let selector = Template.instance().selector;
        return GameTeamsPlayers.find(selector).count() > 0;
    }
});

Template.newTeam.events({
    'submit #addTeam'(event){
        event.preventDefault();
        let gameId = Template.parentData(1)._id;
        let teamName = event.target.teamName.value;

        Meteor.call('addTeam', gameId, teamName, (error) => {
            if (error) {
                throwError(error)
            } else {
                event.target.teamName.value = '';
            }
        })
    },
    'click #cancelAdd'(){
        event.preventDefault();
        let gameId = Template.parentData(1)._id;
        Meteor.call('cancelAddTeam', gameId, (error) => {
            if (error) {
                throwError(error)
            }
        })
    }
})

Template.newTeam.created = function(){
    this.selector = {pendingAddToNewTeam: true, usersAddingToNewTeam: Meteor.userId()};
}