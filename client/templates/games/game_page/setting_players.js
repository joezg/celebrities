Template.settingPlayers.events({
    'submit #join'(event){
        event.preventDefault();

        addPlayer.bind(this)(true, event, Template.instance());
    },
    'submit #addGuest'(event){
        event.preventDefault();

        addPlayer.bind(this)(false, event, Template.instance());
    },
    'click .remove.player.button'(event){
        GameTeamsPlayers.remove(this._id);
    },
    'click #start': function(){
        Meteor.call('startGame', this._id, (error) => {
            // display the error to the user and abort
            if (error)
                return throwError(error);
        })
    },
    'input input'(event){
        if (event.target.id === 'myName'){
            Template.instance().myNameValue.set(event.target.value);
        } else if (event.target.id === 'guestName'){
            Template.instance().guestNameValue.set(event.target.value);
        }
    }
});

Template.settingPlayers.helpers({
    isUserEntered(){
        return GameTeamsPlayers.find({isUser: true, creatorId: Meteor.userId()}).count() > 0;
    },
    teams(){
        let uniqueTeamNames = _.uniq(GameTeamsPlayers.find({}, {
            sort: {teamName: 1}, fields: {teamName: true}
        }).fetch().map(function(doc) {
            return doc.teamName;
        }), true);

        uniqueTeamNames.push(ADD_NEW_TEAM_NAME);

        return _.map(uniqueTeamNames, name => {
            let data = {
                name: name,
                isTeamPlaceholder: false,
                isUnsortedPlayersTeam: false
            }

            if (name === ADD_NEW_TEAM_NAME) { //this is a placeholder team for entering a new team
                data.isTeamPlaceholder = true;
            } else if (!name){
                data.isUnsortedPlayersTeam = true;
            }

            return data;
        });
    },
    canEnterMyself(){
        return !!Template.instance().myNameValue.get();
    },
    canEnterGuest(){
        return !!Template.instance().guestNameValue.get();
    },
    myName(){
       return (user = Meteor.user()) && user.profile && user.profile.preferredName ? user.profile.preferredName : getUserName(user);
    }
});

Template.settingPlayers.onCreated(function(){
    let instance = this;

    instance.autorun(()=>{
        instance.subscribe('gameTeamsPlayers', instance.data._id)
    })

    instance.myNameValue = new ReactiveVar(true);
    instance.guestNameValue = new ReactiveVar(null);
})

let addPlayer = function(isUser, event, instance){
    let playerName = event.target.playerName.value;

    //do not enter empty name
    if (!playerName)
        return;

    Meteor.call('addPlayer', this._id, playerName, isUser, error => {
        if (error) {
            throwError(error)
        } else {
            event.target.playerName.value = '';
        }
        instance.myNameValue.set(true); //because field is removed from DOM when entered and has a default value when added again
        instance.guestNameValue.set(null);
    });
}

const ADD_NEW_TEAM_NAME = "Add to new team...";