/* global Games isDocumentCreator */
Template.participants.events({
    'change #public_joining_allowed': function(){
        let id = this._id;
        let publicJoiningAllowed = !this.preferences.publicJoiningAllowed;

        if (publicJoiningAllowed){
            Meteor.call('gameAllowJoining', id, function(error, result){
                // display the error to the user and abort
                if (error)
                    return throwError(error);
            });
        } else {
            Meteor.call('gameDisableJoining', id, function(error, result){
                // display the error to the user and abort
                if (error)
                    return throwError(error);
            });
        }


    },

    'submit #invite': function(event) {
        event.preventDefault();
        let newUsername = event.target.userName.value;
        let id = this._id;

        Meteor.call('gameAddUser', id, newUsername, function(error, result) {
            // display the error to the user and abort
            if (error)
                return throwError(error);
        });

        event.target.userName.value = '';
    }
});