Template.waitingUserAcceptPlay.helpers({
    playerName(){
        let gs = GameState.findOne(this._id);

        return GameTeamsPlayers.findOne({
            gameId: this._id,
            playerOrder: gs.currentPlayer,
            teamOrder: gs.currentTeam
        }).playerName
    }
});
Template.waitingUserAcceptPlay.events({
    'click #playerReady'(){
        Meteor.call('userReady', this._id, (error) => {
            if (error)
                throwError(error);
        })
    }
});

Template.waitingUserAcceptPlay.onCreated(function(){
    let instance = this;

    instance.autorun(()=>{
        instance.subscribe('gameTeamsPlayers',instance.data._id );
    })
});