/* global Games */
Template.rules.helpers({
    selectionMethodByEntering(){
        return this.preferences.selectionMethod == 'entering' ? 'checked' : '';
    },
    selectionMethodByPicking(){
        return this.preferences.selectionMethod == 'picking' ? 'checked' : '';
    },
    error(){
        return !!Session.get('gamePageRulesSubmitError') ? 'error' : '';
    },
    errorMessage(){
        return Session.get('gamePageRulesSubmitError');
    }

});

Template.rules.events({
    'change .selectionMethod': function(event, template){
        let element = template.find('input:radio[name=selectionMethod]:checked');
        let selectionMethod = $(element).val();
        Games.update({_id: this._id}, {$set: {"preferences.selectionMethod": selectionMethod}})
    }
});