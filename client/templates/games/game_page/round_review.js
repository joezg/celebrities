Template.roundReview.helpers({
    corrections(){
        return getCorrections(this._id);
    },
    isThereAnyCorrection(){
        return anyCorrection(this._id);
    },
    wrongs(){
        return getWrongs(this._id);
    },
    isThereAnyWrongs(){
        return anyWrongs(this._id);
    },
    disputes(){
        return getDisputes(this._id);
    },
    isThereAnyDisputes(){
        return anyDisputes(this._id);
    },
    corrects(){
        return getCorrects(this._id);
    },
    isThereAnyCorrects(){
        return anyCorrects(this._id);
    },
    isUserGuessing(){
        return isUserGuessing(Meteor.userId(), this.gameId ? this.gameId : this._id);
    },
    isThereAnythingToDo(){
        return anyDisputes(this._id) ||
               anyCorrection(this._id);
    },
    finishButtonDisabled(){
        return anyDisputes(this._id) ||
               anyCorrection(this._id) ? 'disabled' : '';
    },
    currentScore(){
        //determine this round score to pass it to game_result template
        let corrects = getCorrects(this._id);
        let gs = GameState.findOne(this._id);
        let currentTeamName = gs.currentTeamName;

        return {
            team: currentTeamName,
            score: corrects.count(),
            phase: gs.phase,
            _id: this._id //this is needed if gameResult template receives this object as data instead of parent object it normally receives
        }
    }
});

Template.roundReview.events({
    'click .dispute.accept'(){
        Meteor.call('respondOnDispute', this._id, true, (error)=>{
            if (error)
                throwError(error);
        })
    },
    'click .dispute.reject'(){
        Meteor.call('respondOnDispute', this._id, false, (error)=>{
            if (error)
                throwError(error);
        })
    },
    'click .correction.accept'(){
        Meteor.call('respondOnCorrection', this._id, true, (error)=>{
            if (error)
                throwError(error);
        })
    },
    'click .correction.reject'(){
        Meteor.call('respondOnCorrection', this._id, false, (error)=>{
            if (error)
                throwError(error);
        })
    },
    'click .correction.propose'(){
        Meteor.call('addCorrection', this._id, (error)=>{
            if (error)
                throwError(error);
        })
    },
    'click .dispute.button'(){
        Meteor.call('dispute', this._id, (error)=>{
            if (error){
                throwError(error);
            }
        });
    },
    'click .finish.button'(){
        Meteor.call('completeRound', this._id, (error)=>{
            if (error){
                throwError(error);
            }
        });
    }
})

let getCorrections = function(gameId){
    return GameCards.find({
        gameId: gameId,
        correctionProposed: true,
        guessed: false
    });
}

let anyCorrection = function(gameId){
    return getCorrections(gameId).count() > 0;
}

let getWrongs = function(gameId){
    return GameCards.find({
        gameId: gameId,
        guessed: false,
        correctionProposed: {$ne: true}
    })
}

let anyWrongs = function(gameId){
    return getWrongs(gameId).count() > 0;
}

let getDisputes = function(gameId){
    return GameCards.find({
        gameId: gameId,
        disputed: true,
        guessed: true
    });
}

let anyDisputes = function(gameId){
    return getDisputes(gameId).count() > 0;
}

let getCorrects = function(gameId){
    return GameCards.find({
        gameId: gameId,
        guessed: true,
        disputed: {$ne: true},
        wonInPreviousRound: {$ne: true}
    });
}

let anyCorrects = function(gameId){
    return getCorrects(gameId).count() > 0;
}

Template.roundReview.onCreated(function(){
    let instance = this;

    instance.autorun(()=>{
        instance.subscribe('gameCardsGuessed', instance.data._id);
        instance.subscribe('gameCardsWrong', instance.data._id);
        instance.subscribe('gameCardsCorrectionProposed', instance.data._id);
    })
});
