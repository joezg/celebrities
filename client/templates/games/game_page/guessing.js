Template.guessing.helpers({
    cardName(){
        let currentCard = GameCards.findOne({
            gameId: this._id,
            current: true
        });

        return currentCard ? currentCard.name : "" ;
    },
    currentTime(){
        let countdown = GetCountdown(this._id);
        return countdown ? countdown.get() : 0;
    },
    playerName(){
        let gs = GameState.findOne(this._id);

        if (gs){
            let player = GameTeamsPlayers.findOne({
                gameId: this._id,
                playerOrder: gs.currentPlayer,
                teamOrder: gs.currentTeam,
            })
            return player ? player.playerName : "";
        } else {
            return "";
        }
    },
    userGuessing(){
        let gs = GameState.findOne(this._id);

        return isUserGuessing(Meteor.userId(), this._id)
    },
    guesses(){
        return GameCards.find({
            gameId: this._id,
            guessed: true,
            wonInPreviousRound: {$ne: true}
        })
    },
    remainingCards(){
        return GameCards.find({
            gameId: this._id,
            guessed: {$ne: true},

        }).count() - 1;
    }
})

Template.guessing.events({
    'click .guess.ok'(){
        Meteor.call('guess', this._id, (error)=>{
            if (error){
                throwError(error);
            }
        });
    },
    'click .guess.wrong'(){
        Meteor.call('skip', this._id, (error)=>{
            if (error){
                throwError(error);
            }
        });
    },
    'click .dispute.button'(){
        Meteor.call('dispute', this._id, (error)=>{
            if (error){
                throwError(error);
            }
        });
    }
});

Template.guessing.onCreated(function(){
    let instance = this;

    instance.autorun(()=>{
        instance.subscribe('gameTeamsPlayers',instance.data._id );
        instance.subscribe('gameCardsCurrent', instance.data._id);
        instance.subscribe('gameCardsGuessed', instance.data._id);
        instance.subscribe('gameCardsConcealed', instance.data._id);
    });

    let state = GameState.findOne(instance.data._id);
    instance.data.isFirstPhase = state.phase === 1;
    instance.data.isSecondPhase = state.phase === 2;
    instance.data.isThirdPhase = state.phase === 3;

    StartCountdown(instance.data._id);
});