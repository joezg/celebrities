Template.phaseReview.helpers({
    cards(){
        let sort = { sort: {} };
        sort.sort[this.phaseNumber+'WonByPlayerId'] = 1;
        return GameCards.find({gameId: this._id}, sort);
    },
    playerName(phaseNumber){
        return GameTeamsPlayers.findOne(this[phaseNumber+'PhaseWonByPlayerId']).playerName;
    },
    teamColor(teamName){
        return getTeamColor(teamName);
    },
    teamThatWonColor(phaseNumber){
        let teamName = this[phaseNumber+'PhaseWonByTeam']
        return getTeamColor(teamName);
    },
    teamThatWonName(phaseNumber){
        return this[phaseNumber+'PhaseWonByTeam']
    }
});

Template.phaseReview.events({
    "click #nextPhase"(){
        Meteor.call('donePhaseReview', this._id, (error)=> {
            if (error)
                throwError(error);
        })
    }
})

Template.phaseReview.onCreated(function(){
    let instance = this;

    instance.autorun(()=>{
        instance.subscribe('gameCardsPhaseReview',instance.data._id );
        instance.subscribe('gameTeamsPlayers',instance.data._id );
    })

    //phaseNumber
    let gs = GameState.findOne(instance.data._id);

    switch (gs.phase){
        case 1:
            instance.data.phaseNumber = 'first';
            break;
        case 2:
            instance.data.phaseNumber = 'second';
            break;
        case 3:
            instance.data.phaseNumber = 'third';
            break;
    }

});