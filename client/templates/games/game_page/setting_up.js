/* global Games isDocumentCreator */
Template.settingUp.events({
    'click #delete': function() {
        Games.remove(this._id);

        Router.go('/');
    },
    'click #done': function(){
        let totalNumberOfCards = parseInt($('input[name=totalNumberOfCards]').val());
        let numberOfCardsPerPlayer = parseInt($('input[name=numberOfCardsPerPlayer]').val());

        let preferences = {totalNumberOfCards, numberOfCardsPerPlayer};

        let errorMessage = validateGameSet(preferences);
        Session.set("gamePageRulesSubmitError", errorMessage);
        if (errorMessage)
            return;

        Meteor.call('setGame', this._id, preferences, (error) => {
            // display the error to the user and abort
            if (error)
                return throwError(error);
        })
    },
    'input input[name=totalNumberOfCards]'(event){
        if (event.target.value){
            $('input[name=numberOfCardsPerPlayer]').val("");
        }
    },
    'input input[name=numberOfCardsPerPlayer]'(event){
        if (event.target.value){
            $('input[name=totalNumberOfCards]').val("");
        }
    }
});

Template.settingUp.helpers({
    isGameCreator(){
        return isDocumentCreator(Meteor.userId(), this);
    }
})