let numberOfCardsPerPlayer = function(){
    return Games.findOne(this.gameId).preferences.numberOfCardsPerPlayer;
}

Template.playerPickingCards.helpers({
    teamColor(teamName){
        return getTeamColor(teamName);
    },
    cards(){
        return GameCards.find({playerIds: this._id});
    },
    numberOfCardsPerPlayer,
    canComplete(){
        return GameCards.find({playerIds: this._id}).count() === numberOfCardsPerPlayer.bind(this)();
    },
    canEnter(){
        return !!Template.instance().cardNameValue.get();
    }
});

Template.playerPickingCards.events({
    'submit #addCard'(event){
        event.preventDefault();

        //do not enter empty name
        if (!event.target.cardName.value)
            return;

        Meteor.call('addCard', this._id, event.target.cardName.value, (error) => {
            if (error)
                throwError(error);
        })
        event.target.cardName.value = '';
        Template.instance().cardNameValue.set(null);
    },
    'click #finish'(event){
        Meteor.call('finishPickingCards', this._id, (error) => {
            if (error)
                throwError(error);
        })
    },
    'click .remove.name.button'(){
        GameCards.remove(this._id);
    },
    'input #cardName'(event){
        Template.instance().cardNameValue.set(event.target.value);
    }
});

Template.playerPickingCards.onRendered(function(){
    let instance = this;

    let cards = GameCards.find({playerIds: this.data._id});
    let $progress = this.$('#progress');
    $progress.progress({
        value:0
    });

    let numberOfCardsPerPlayer = Games.findOne(instance.data.gameId).preferences.numberOfCardsPerPlayer;

    cards.observeChanges({
        added(){
            $progress.progress('increment');
        },
        removed(){
            if ($progress.data().moduleProgress.percent !== 100 || this.cursor.count() < numberOfCardsPerPlayer){
                $progress.progress('increment',-1);
            }
        }
    })

});

Template.playerPickingCards.onCreated(function(){
    let instance = this;

    instance.autorun(()=>{
        instance.subscribe('gameCards', instance.data.gameId, instance.data._id);
    })

    instance.cardNameValue = new ReactiveVar(null);
})