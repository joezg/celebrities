/* global Games */
Template.gameList.helpers({
    games() {
        return Games.find({}, {sort: {date: -1}});
    },
    gameExists() {
        return Games.find({}, {sort: {date: -1}}).count() > 0;
    }
});

Template.gameItem.events({
    'click .game-item': function() {
        Router.go('gamePage', this);
    }
})