Accounts.emailTemplates.from = "Celebrities game <no-reply@celebrities-game.com>";
Accounts.emailTemplates.siteName = "Celebrities Game";
Accounts.emailTemplates.verifyEmail.subject = function() {
    return "Please, verify your email";
};
Accounts.emailTemplates.verifyEmail.text = function(user, url){
    return `Hi, ${user.username}\nTo verify your account, simply click the link below:\n\n${url}`;
};