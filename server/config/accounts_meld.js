AccountsMeld.configure({
    meldDBCallback(srcUserId, destUserId){
        let destUser = Meteor.users.findOne(destUserId),
            name = destUser.profile && destUser.profile.name ? destUser.profile.name : destUser.username;
        GameCards.update(
            {$or: [ {creatorId: srcUserId}, {creatorId: destUserId}]},
            {$set: {creatorId: destUserId, creator: name}},
            {multi: true}
        );
        GameTeamsPlayers.update(
            {$or: [ {creatorId: srcUserId}, {creatorId: destUserId}]},
            {$set: {creatorId: destUserId, creator: name}},
            {multi: true}
        );
        Games.update(
            {$or: [ {creatorId: srcUserId}, {creatorId: destUserId}]},
            {$set: {creatorId: destUserId, creator: name}},
            {multi: true}
        );

        //where old user is in game, add new user
        Games.update(
            {"users._id": srcUserId},
            {
                $push: {users: {_id: destUserId, username: name}}
            },
            {multi: true}
        );

        //where old user is in game, remove it
        Games.update(
            {"users._id": srcUserId},
            {
                $pull: {users: {_id: srcUserId}}
            },
            {multi: true}
        );

        //where new user is in game, update username
        Games.update(
            {"users._id": destUserId},
            {
                $set: {"users.$.username": name}
            },
            {multi: true}
        );
    }
})