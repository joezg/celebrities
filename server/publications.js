let userOrConnectionId = function(context){
    return [
        {"users._id": context.userId},
        {anonymousConnectionIds: context.connection.id}
    ]
}

Meteor.publish('games', function(){
    return Games.find({
        $or: userOrConnectionId(this)
    });
});

Meteor.publish('gameState', function(gameId){
    check(gameId, String);
    if (Games.findOne({_id: gameId, $or: userOrConnectionId(this)})){
        return GameState.find({_id: gameId});
    } else {
        this.ready(); //empty cursor
    }
});

Meteor.publish('gameTeamsPlayers', function(gameId){
    check(gameId, String);
    if (Games.findOne({_id: gameId, $or: userOrConnectionId(this)})){
        return GameTeamsPlayers.find({gameId: gameId});
    } else {
        this.ready(); //empty cursor
    }
});

Meteor.publish('gameCards', function(gameId, playerId){
    check(gameId, String);
    check(playerId, String);

    if (Games.findOne({_id: gameId, $or: userOrConnectionId(this)})){
        let gtp = GameTeamsPlayers.findOne({gameId: gameId, _id: playerId});
        if (gtp){
            if (gtp.userPickingCards === this.userId && !gtp.isPickingCardsDone && gtp.isPickingCardsInProgress){
                return GameCards.find({
                    gameId: gameId,
                    playerIds: playerId
                });
            }
        }

    }

    //else
    this.ready(); //empty cursor
});

Meteor.publish("gameCardsConcealed", function (gameId) {
    check(gameId, String);

    if (Games.findOne({_id: gameId, "users._id": this.userId})){
        return GameCards.find({gameId: gameId}, {fields: {playerIds: 1, guessed: 1, gameId: 1}});

    }

    //else
    this.ready();
});

Meteor.publish("gameCardsCurrent", function (gameId) {
    check(gameId, String);

    if (isUserGuessing(this.userId, gameId))
        return GameCards.find({gameId: gameId, current: true})

    //else
    this.ready();
});

Meteor.publish("gameCardsWrong", function (gameId) {
    check(gameId, String);

    if (isUserGuessing(this.userId, gameId))
        return GameCards.find({gameId: gameId, guessed: false})

    //else
    this.ready();
});

Meteor.publish("gameCardsGuessed", function (gameId) {
    check(gameId, String);

    return GameCards.find({gameId: gameId, guessed: true});
});

Meteor.publish("gameCardsCorrectionProposed", function(gameId){
    check(gameId, String);

    return GameCards.find({gameId: gameId, correctionProposed: true});
});

Meteor.publish("gameCardsPhaseReview", function(gameId){
    check(gameId, String);

    let fsm = GameStateMachine(gameId);

    if (fsm.is('phaseReview') || fsm.is("gameReview")){
        return GameCards.find({gameId: gameId});
    }

    //else
    this.ready();
});

Meteor.publish("gameScore", function(gameId){
    check(gameId, String);

    let fsm = GameStateMachine(gameId);

    return GameCards.find({gameId: gameId}, {
        fields: {
            firstPhaseWonByPlayerId: 1,
            secondPhaseWonByPlayerId: 1,
            thirdPhaseWonByPlayerId: 1,
            firstPhaseWonByTeam: 1,
            secondPhaseWonByTeam: 1,
            thirdPhaseWonByTeam: 1,
            gameId: 1
        }
    });
});