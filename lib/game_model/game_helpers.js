getOrderCandidates = function(gameId, teamOrder, playerOrder){
    if (!teamOrder || !playerOrder){
        let gs = GameState.findOne(gameId);

        if (gs){
            teamOrder = gs.currentTeam;
            playerOrder = gs.currentPlayer;
        }
    }

    if (teamOrder && playerOrder){
        return GameTeamsPlayers.find({
            gameId: gameId,
            $or: [
                { teamOrder: teamOrder, playerOrder: playerOrder },
                { teamOrder: {$exists: false}, playerOrder: {$exists: false} },
                { teamOrder: teamOrder, playerOrder: {$exists: false} },
            ]
        });
    }

    return [];
}

isEveryUserDonePickingCards = function(_id, game){
    let players = GameTeamsPlayers.find({gameId: _id});

    if (!game){
        game = Games.findOne(_id);
    }

    let isDone = true;
    players.forEach((player)=> {
        var playerCardsCount = GameCards.find({
            gameId: _id,
            playerIds: player._id
        }).count();

        if (playerCardsCount !== game.preferences.numberOfCardsPerPlayer)
            isDone = false;
    });

    return isDone;
}