AccountsTemplates.configure({
    defaultLayout: 'layout',
    overrideLoginErrors: false,
    confirmPassword: false,
    enablePasswordChange: true,
    showForgotPasswordLink: true,
    sendVerificationEmail: true
});

var pwd = AccountsTemplates.removeField('password');
var email = AccountsTemplates.removeField('email');
AccountsTemplates.addFields([
    {
        _id: "username",
        type: "text",
        displayName: "username",
        required: true,
        minLength: 5
    },
    email,
    {
        _id: 'username_and_email',
        type: 'text',
        required: true,
        displayName: "Login",
        placeholder: "Username or email"
    },
    pwd
]);

//*** ACCOUNTS ROUTES **//
AccountsTemplates.configureRoute('signIn');
AccountsTemplates.configureRoute('signUp');
AccountsTemplates.configureRoute('changePwd');