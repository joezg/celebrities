/* global Games */
Router.configure({
    layoutTemplate: 'layout',
    loadingTemplate: 'loading',
    notFoundTemplate: 'notFound'
});

Router.route('/', { name: 'gameMenu' });
Router.route('/game/:_id', {
    name: 'gamePage'
});

//securing routes to logged-in only
Router.plugin('ensureSignedIn', {
    except: ['gameMenu', 'atSignIn', 'atSignUp', 'atForgotPassword', 'gamePage']
});