let countdowns = {

};

StartCountdown = function(gameId){
    if (!countdowns[gameId]){
        countdowns[gameId] = new ReactiveCountdown(30)
    }

    StopCountdown(gameId);

    countdowns[gameId].start(()=>{
        if (Meteor.isServer){
            GameStateMachine(gameId).timesOut();
        }
    });
}

GetCountdown = function(gameId){
    return countdowns[gameId];
}

StopCountdown = function(gameId){
    let cd = countdowns[gameId];

    cd && cd.stop();
}