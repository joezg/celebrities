/* global Games isDocumentCreator  */

Games = new Mongo.Collection('games');

Games.allow({
    remove: function(userId, game) {
        return isDocumentCreator(userId, game);
    },
    update: function(userId, game) {
        return isDocumentCreator(userId, game);
    }
})

Games.deny({
    update: function(userId, game, fieldNames) {
        return _.without(fieldNames, 'location', 'preferences').length > 0
    }
});

Meteor.methods({
    gameInsert: function(game) {
        check(Meteor.userId(), String);
        check(game, {
            location: String
        });

        game = formCreatedDocument(_.extend(game, {
            users: [prepareUser(Meteor.user())],
            date: new Date(),
            preferences: {
                publicJoiningAllowed: false,
                selectionMethod: "entering",
                totalNumberOfCards: 40
            }
        }));
        var gameId = Games.insert(game);

        if (Meteor.isServer) {
            GameStateMachine(gameId).create();
        }

        return {
            _id: gameId
        };
    },
    gameAddUser: function(id, username) {
        check(Meteor.userId(), String);
        check(id, String);
        check(username, String);

        if (Meteor.isServer) {
            let game = Games.findOne(id);

            if (!game) {
                throw new Meteor.Error("Game doesn't exists", `A game with id ${id} doesn't exists!`);
            } else if (!isDocumentCreator(Meteor.userId(), game)) {
                if (game.preferences.publicJoiningAllowed && username != Meteor.user().username) {
                    throw new Meteor.Error('access-denied', 'Cannot add user other than yourself!');
                } else if (!game.preferences.publicJoiningAllowed) {
                    throw new Meteor.Error('access-denied', "You cannot join a game that doesn't have public access.");
                }
            }

            if (!GameStateMachine(id).is('settingUp')) {
                throw new Meteor.Error('wrong-state', 'Game is not in settingUp state');
            }

            let profileNameRegex = {
                $regex: '^'+username+'$',
                $options: 'i'
            }

            let user = Meteor.users.findOne({
                $or: [
                    {"username": username},
                    {"profile.name": profileNameRegex}
                ]
            });
            if (!user)
                throw new Meteor.Error('user not exists', `User ${username} doesn't exists!`);

            Games.update({_id: id}, {$push: {users: prepareUser(user)}});

            return {
                _id: game._id
            };
        } else {
            Games.update({_id: id}, {$push: {users: {_id: null, username: username}}});
        }
    },
    gameAllowJoining: function(id) {
        check(Meteor.userId(), String);
        check(id, String);

        let game = Games.findOne(id);

        if (Meteor.isServer) {
            if (!game) {
                throw new Meteor.Error("Game doesn't exists", `A game with id ${id} doesn't exists!`);
            } else if (!isDocumentCreator(Meteor.userId(), game)) {
                throw new Meteor.Error('access-denied', "You cannot change a game you didn't create.");
            }

            if (!GameStateMachine(id).is('settingUp')) {
                throw new Meteor.Error('wrong-state', 'Game is not in settingUp state');
            }

            //determing joining code
            let numberOfLetters = 4;
            let joiningId = null;
            while (!joiningId){
                let potentialId = game._id.substr(0, numberOfLetters++).toUpperCase();
                if (Games.find({"preferences.joiningId": potentialId}, {_id: 1}).count() === 0){
                    joiningId = potentialId;
                }
            }

            Games.update({_id: id}, {$set: {"preferences.publicJoiningAllowed": true, "preferences.joiningId": joiningId}});
        } else {
            Games.update({_id: id}, {$set: {"preferences.publicJoiningAllowed": true, "preferences.joiningId": ""}});
        }
    },
    gameDisableJoining: function(id) {
        check(Meteor.userId(), String);
        check(id, String);

        let game = Games.findOne(id);

        if (Meteor.isServer) {
            if (!game) {
                throw new Meteor.Error("Game doesn't exists", `A game with id ${id} doesn't exists!`);
            } else if (!isDocumentCreator(Meteor.userId(), game)) {
                throw new Meteor.Error('access-denied', "You cannot change a game you didn't create.");
            }

            if (!GameStateMachine(id).is('settingUp')) {
                throw new Meteor.Error('wrong-state', 'Game is not in settingUp state');
            }

        }

        Games.update({_id: id}, {$set: {"preferences.publicJoiningAllowed": false}, $unset:{"preferences.joiningId": true}});
    },
    gameJoinByCode: function(code) {
        check(code, String);

        let game = Games.findOne({"preferences.joiningId": code.toUpperCase(), "preferences.publicJoiningAllowed": true });

        if (Meteor.isServer) {
            if (!game) {
                throw new Meteor.Error("not_exists", `A game with code ${code} doesn't exists or is not allowed to join!`);
            }

            if (this.userId){
                Games.update({_id: game._id}, {$push: {users: prepareUser(Meteor.user())}});
            } else {
                Games.update({_id: game._id}, {$push: {anonymousConnectionIds: this.connection.id }});
            }


            return {
                _id: game._id
            };

        }
    },
    setGame: function(_id, preferences) {
        check(_id, String);
        check(preferences, {
            totalNumberOfCards: Match.Optional(Number),
            numberOfCardsPerPlayer: Match.Optional(Number)
        });

        if (Meteor.isServer) {
            //validation on client is in event callback because of a clarity
            var validationError = validateGameSet(preferences);
            if (validationError) {
                throw new Meteor.Error("Game set validation error", validationError);
            }

            let game = Games.findOne(_id);

            if (!game) {
                throw new Meteor.Error("Game doesn't exists", `A game with id ${_id} doesn't exists!`);
            } else if (!isDocumentCreator(Meteor.userId(), game)) {
                throw new Meteor.Error("access-denied", "Your are not creator of the game");
            }
        }
        let modification = {
            $set: {},
            $unset: {}
        };

        let fsm = GameStateMachine(_id);

        if (!fsm.is('settingUp')) {
            throw new Meteor.Error('wrong-state', 'Game is not in settingUp state');
        }

        if (preferences.totalNumberOfCards) {
            modification.$set["preferences.totalNumberOfCards"] = preferences.totalNumberOfCards;
            modification.$unset['preferences.numberOfCardsPerPlayer'] = "";
        } else {
            modification.$set["preferences.numberOfCardsPerPlayer"] = preferences.numberOfCardsPerPlayer;
            modification.$unset['preferences.totalNumberOfCards'] = "";
        }

        Games.update({_id: _id}, modification);
        fsm.set();
    },
    startGame(_id){
        check(_id, String);

        if (Meteor.isServer) {
            let game = Games.findOne(_id);

            if (!game) {
                throw new Meteor.Error("Game doesn't exists", `A game with id ${_id} doesn't exists!`);
            } else if (!isDocumentCreator(Meteor.userId(), game)) {
                throw new Meteor.Error("access-denied", "Your are not creator of the game");
            }

            let fsm = GameStateMachine(_id);
            if (!fsm.is('settingPlayers')) {
                throw new Meteor.Error('wrong-state', 'Game is not in settingPlayers state');
            }

            let teams = GameTeamsPlayers.aggregate([
                {$match: {gameId: _id}},
                {$group: {_id: "$teamName", players: {$push: "$playerName"}}}
            ]);

            if (teams.length < 2) {
                throw new Meteor.Error("Game not properly set", "You need to add at least two teams");
            }

            let totalNumberOfPlayers = 0;
            teams.forEach(team => {
                if (team._id === null) {
                    throw new Meteor.Error("Game not properly set", "You have some unsorted players");
                }

                if (team.players.length < 2) {
                    throw new Meteor.Error("Game not properly set", `You need to add at least two players to team ${team._id}`);
                }
                totalNumberOfPlayers += team.players.length;
            });

            //calculate number of cards
            if (game.preferences.totalNumberOfCards) {
                let modification = {$set: {}};
                modification.$set["preferences.numberOfCardsPerPlayer"] = Math.floor(game.preferences.totalNumberOfCards / totalNumberOfPlayers);

                Games.update({_id: _id}, modification);
            }

            fsm.playersSet();
        }
    },
    completeRound(_id){
        check(_id, String);

        if (Meteor.isServer) {
            let game = Games.findOne(_id);

            if (!game) {
                throw new Meteor.Error("not-exists", `A game with id ${_id} doesn't exists!`);
            }

            if (!isUserInGame(Meteor.userId(), _id)){
                throw new Meteor.Error("access-denied", 'You are not part of this game');
            }

            let fsm = GameStateMachine(_id);
            if (!fsm.is('roundReview')) {
                throw new Meteor.Error('wrong-state', 'Game is not in roundReview state');
            }

            if (GameCards.find({
                    gameId: _id,
                    $or: [
                        {disputed: true},
                        {correctionProposed: true}
                    ]
                }).count() > 0){
                throw new Meteor.Error('wrong-state', 'You still have disputed / correction proposed names');
            }

            fsm.finishRound();
        }
    },
    donePickingCards(_id){
        check(_id, String);

        if (Meteor.isServer) {
            let game = Games.findOne(_id);

            if (!game) {
                throw new Meteor.Error("Game doesn't exists", `A game with id ${_id} doesn't exists!`);
            }

            let fsm = GameStateMachine(_id);
            if (!fsm.is('pickingCards')) {
                throw new Meteor.Error('wrong-state', 'Game is not in pickingCards state');
            }

            if (!isEveryUserDonePickingCards(_id))
                throw new Meteor.Error("wrong-state", `Some users still need to enter ${game.preferences.numberOfCardsPerPlayer} cards`);

            fsm.cardsPicked();
        }
    },
    donePhaseReview(_id){
        check(_id, String);

        if (Meteor.isServer) {
            let game = Games.findOne(_id);

            if (!game) {
                throw new Meteor.Error("not-exists", `A game with id ${_id} doesn't exists!`);
            }

            if (!isUserInGame(Meteor.userId(), _id)){
                throw new Meteor.Error("access-denied", 'You are not part of this game');
            }

            let fsm = GameStateMachine(_id);
            if (!fsm.is('phaseReview')) {
                throw new Meteor.Error('wrong-state', 'Game is not in phaseReview state');
            }

            fsm.finishPhase();
        }
    },
    adminResetPlay(gameId){
        check(gameId, String);

        if (Meteor.isServer && !Meteor.user().isAdmin){
            throw new Meteor.Error('access-denied', 'admin only');
        }

        GameCards.update(
            {gameId: gameId},
            {
                $unset: {guessed: true, skip:true, firstPhaseWonByPlayerId:true, firstPhaseWonByTeam:true, wonInPreviousRound:true, secondPhaseWonByPlayerId:true, secondPhaseWonByTeam:true, thirdPhaseWonByPlayerId:true, thirdPhaseWonByTeam:true }
            }, {multi: true});

        GameState.update(
            {_id: gameId}, {
            $unset:{
                currentPlayer: true,
                currentTeam: true,
                currentTeamName: true,
                currentPlayerName: true,
                userGuessing: true,
                lastPlayerTeam_1: true,
                lastPlayerTeam_2: true,
                phase: true
            },
            $set:{
                state: 'pickingCards'
            }
        }, {multi: true});

        GameTeamsPlayers.update(
            {gameId: gameId}, {
            $unset:{
                playerOrder: true,
                teamOrder: true
            }
        }, {multi: true});
    }
});

validateGameSet = function(preferences) {
    if (!preferences.totalNumberOfCards && !preferences.numberOfCardsPerPlayer) {
        return "You need to enter number of cards";
    }

    if (!!preferences.totalNumberOfCards && !!preferences.numberOfCardsPerPlayer) {
        return "Only one number of cards should be set";
    }

    return null;
}
var prepareUser = (user) => {
    return {
        _id: user._id,
        username: getUserName(user)
    }
}