formCreatedDocument = function(document){
    let user = Meteor.user()
    document.creatorId = user._id;
    document.creator = getUserName(user);
    return document;
}
