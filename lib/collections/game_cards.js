GameCards = new Mongo.Collection('gameCards');

GameCards.allow({
    remove: function(userId, gameCard){
        return isDocumentCreator(userId, gameCard);
    }
});

Meteor.methods({
    addCard(playerId, cardName){
        check(Meteor.userId(), String);
        check(playerId, String);
        check(cardName, String);

        let gtp = GameTeamsPlayers.findOne(playerId);

        //don't have to check whether user is in game because userPickingCards

        if (!gtp){
            throw new Meteor.Error("not-exists", `player with ${playerId} doesn't exists`);
        }

        if (!GameStateMachine(gtp.gameId).is('pickingCards')){
            throw new Meteor.Error('wrong-state', 'Game is not in pickingCards state');
        }

        if (gtp.userPickingCards !== Meteor.userId()){
            throw new Meteor.Error("access-denied", `Another user already picking cards for ${gtp.playerName}` );
        }

        if (gtp.isPickingCardsDone){
            throw new Meteor.Error("access-denied", `${gtp.playerName} already picked cards` );
        }

        let regex = {
            $regex: '^'+cardName+'$',
            $options: 'i'
        }

        let card = GameCards.findOne({
            gameId: gtp.gameId,
            name: regex
        });

        if (!card){
            GameCards.insert(formCreatedDocument({
                gameId: gtp.gameId,
                playerIds: [playerId],
                name: cardName
            }));
        } else {
            GameCards.update({
                _id: card._id
            }, {
                $push: { playerIds: playerId }
            })
        }

    },
    guess(gameId){
        check(Meteor.userId(), String);
        check(gameId, String);

        let fsm = GameStateMachine(gameId);
        if (!fsm.is('guessing')){
            throw new Meteor.Error('wrong-state', 'Game is not in guessing state');
        }

        if (!isUserGuessing(Meteor.userId(), gameId)){
            throw new Meteor.Error("access-denied", `It is not your turn to act in this game`);
        }

        GameCards.update({
            gameId: gameId,
            current:true
        }, {
            $set: {
                guessed: true
            },
            $unset: {
                current: true
            }
        });

        fsm.cardResolved();

    },
    skip(gameId){
        check(Meteor.userId(), String);
        check(gameId, String);

        let fsm = GameStateMachine(gameId);
        if (!fsm.is('guessing')){
            throw new Meteor.Error('wrong-state', 'Game is not in guessing state');
        }

        if (!isUserGuessing(Meteor.userId(), gameId)){
            throw new Meteor.Error("access-denied", `It is not your turn to act in this game`);
        }

        if (GameState.findOne(gameId).phase === 1){
            throw new Meteor.Error("illegal-action", "You cannot skip a card in first phase");
        };

        GameCards.update({
            gameId: gameId,
            current:true
        }, {
            $unset: {
                current: true
            }
        });

        if (Meteor.isServer){
            fsm.cardResolved();
        }

    },
    adminResetCards(gameId){
        check(gameId, String);

        if (Meteor.isServer && !Meteor.user().isAdmin){
            throw new Meteor.Error('access-denied', 'admin only');
        }

        GameCards.update({
            gameId: gameId
        }, {
            $unset: {
                current: true,
                skip: true,
                guessed: true
            }
        }, {
            multi: true
        });
    },
    addCorrection(_id){
        check(Meteor.userId(), String);
        check(_id, String);

        let card = GameCards.findOne(_id);

        if (!card){
            throw new Meteor.Error('not-exists', `Card with id ${_id} doesn't exists`);
        }

        let fsm = GameStateMachine(card.gameId);
        if (!fsm.is('roundReview')){
            throw new Meteor.Error('wrong-state', 'Game is not in roundReview state');
        }

        if (!isUserGuessing(Meteor.userId(), card.gameId)){
            throw new Meteor.Error("access-denied", 'You cannot add correction. Only current player can.');
        }

        GameCards.update({
            _id: _id
        }, {
            $set: {
                correctionProposed: true
            }
        });
    },
    dispute(_id){
        check(Meteor.userId(), String);
        check(_id, String);

        let card = GameCards.findOne(_id);

        if (card.wonInPreviousRound){
            throw new Meteor.Error('wrong-state', 'Dispute is available only on cards won this round');
        }


        if (!card){
            throw new Meteor.Error('not-exists', `Card with id ${_id} doesn't exists`);
        }

        let fsm = GameStateMachine(card.gameId);
        if (!fsm.is('guessing') && !fsm.is('roundReview')){
            throw new Meteor.Error('wrong-state', 'Game is not in guessing or roundReview states');
        }

        if (isUserGuessing(Meteor.userId(), card.gameId)){
            throw new Meteor.Error("access-denied", 'You cannot dispute your own answer.');
        }

        GameCards.update({
            _id: _id
        }, {
            $set: {
                disputed: true
            }
        });
    },
    respondOnDispute(_id, isAccepted){
        check(Meteor.userId(), String);
        check(_id, String);
        check(isAccepted, Boolean);

        let card = GameCards.findOne(_id);

        if (!card){
            throw new Meteor.Error('not-exists', `Card with id ${_id} doesn't exists`);
        }

        if (!card.disputed){
            throw new Meteor.Error('wrong-state', "You cannot respond on dispute on this card because there is no dispute on this card");
        }

        let fsm = GameStateMachine(card.gameId);
        if (!fsm.is('roundReview')){
            throw new Meteor.Error('wrong-state', 'Game is not in roundReview state');
        }

        if (!isUserGuessing(Meteor.userId(), card.gameId)){
            throw new Meteor.Error("access-denied", 'You cannot respond on dispute. Only current player can do that.');
        }

        let modification = {};
        if (isAccepted){
            modification.$set = { guessed: false };
        }

        modification.$unset = { disputed: true };

        GameCards.update({
            _id: _id
        }, modification);
    },
    respondOnCorrection(_id, isAccepted){
        check(Meteor.userId(), String);
        check(_id, String);
        check(isAccepted, Boolean);

        let card = GameCards.findOne(_id);

        if (!card){
            throw new Meteor.Error('not-exists', `Card with id ${_id} doesn't exists`);
        }

        let fsm = GameStateMachine(card.gameId);
        if (!fsm.is('roundReview')){
            throw new Meteor.Error('wrong-state', 'Game is not in roundReview state');
        }

        if (isUserGuessing(Meteor.userId(), card.gameId)){
            throw new Meteor.Error("access-denied", 'You cannot respond on correction. Other players than current player can do that.');
        }

        let modification = {};
        if (isAccepted){
            modification.$set = { guessed: true };
        }

        modification.$unset = { correctionProposed: true };

        GameCards.update({
            _id: _id
        }, modification);
    }
});