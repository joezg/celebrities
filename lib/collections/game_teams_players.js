GameTeamsPlayers = new Mongo.Collection('gameTeamsPlayers');

GameTeamsPlayers.allow({
    update: function(userId, gtp){
        return isUserInGame(userId, gtp.gameId) && GameStateMachine(gtp.gameId).is('settingPlayers');
    },
    remove:function(userId, gtp){
        return isUserInGame(userId, gtp.gameId) && GameStateMachine(gtp.gameId).is('settingPlayers');
    }
});

GameTeamsPlayers.deny({
    update: function(userId, gtp, fieldNames){
        return _.without(fieldNames, 'teamName', 'pendingAddToNewTeam', 'usersAddingToNewTeam').length > 0
    }
})

Meteor.methods({
    addPlayer(gameId, playerName, isUser){
        check(Meteor.userId(), String);
        check(gameId, String);
        check(playerName, String);
        check(isUser, Boolean);

        if (!GameStateMachine(gameId).is('settingPlayers')){
            throw new Meteor.Error('wrong-state', 'Game is not in settingPlayers state');
        }

        if (!isUserInGame(Meteor.userId(), gameId))
            throw new Meteor.Error("access-denied", 'You are not participating in this game');

        let gtp = formCreatedDocument({
            playerName: playerName,
            isUser: isUser,
            gameId: gameId
        });

        GameTeamsPlayers.insert(gtp);

        //save preferred name for user
        if(isUser && playerName !== getUserName(Meteor.user())){
            Meteor.users.update({_id: Meteor.user()._id}, {$set: {"profile.preferredName": playerName}});
        } else {
            Meteor.users.update({_id: Meteor.user()._id}, {$unset: {"profile.preferredName": true}});
        }
    },
    addTeam(gameId, teamName){
        check(Meteor.userId(), String);
        check(gameId, String);
        check(teamName, String);

        if (!GameStateMachine(gameId).is('settingPlayers')){
            throw new Meteor.Error('wrong-state', 'Game is not in settingPlayers state');
        }

        //don't have to check if user is in game because it is already checked when updating a document,
        // preparing it to be added to new team (see allow-update)
        GameTeamsPlayers.update({
            pendingAddToNewTeam: true,
            gameId: gameId,
            usersAddingToNewTeam: Meteor.userId()
        }, {
            $set: {
                pendingAddToNewTeam:false,
                usersAddingToNewTeam: [],
                teamName: teamName//event.target.teamName.value
            }
        }, {
            multi:true
        })

    },
    cancelAddTeam(gameId){
        check(Meteor.userId(), String);
        check(gameId, String);

        //don't have to check state because only in settingPlayers there could be something to update
        //don't have to check if user is in game because it is already checked when updating a document,
        // preparing it to be added to new team (see allow-update)
        GameTeamsPlayers.update({
            pendingAddToNewTeam: true,
            gameId: gameId,
            usersAddingToNewTeam: Meteor.userId()
        }, {
            $set: {
                pendingAddToNewTeam:false,
                usersAddingToNewTeam: []
            }
        }, {
            multi:true
        })

    },
    startPickingCards(_id){
        check(Meteor.userId(), String);
        check(_id, String);
        let gtp = GameTeamsPlayers.findOne(_id);

        if (!gtp){
            throw new Meteor.Error("non-existing-player", `player with id ${_id} doesn't exists`);
        }

        if (!GameStateMachine(gtp.gameId).is('pickingCards')){
            throw new Meteor.Error('wrong-state', 'Game is not in pickingCards state');
        }

        if (!isUserInGame(Meteor.userId(), gtp.gameId)){
            throw new Meteor.Error("access-denied", 'You are not participating in this game');
        }

        if (gtp.isUser && gtp.creatorId !== Meteor.userId()){
            throw new Meteor.Error("access-denied", 'You can start entering games only for guest');
        }

        if (gtp.userPickingCards && gtp.userPickingCards !== Meteor.userId()){
            throw new Meteor.Error("access-denied", `Another user already start picking cards for ${gtp.playerName}`);
        }

        if (gtp.isPickingCardsDone){
            throw new Meteor.Error("access-denied", `${gtp.playerName} already picked cards`);
        }

        if (gtp.isPickingCardsInProgress){
            throw new Meteor.Error("invalid-state", `${gtp.playerName} already picking cards`);
        }

        GameTeamsPlayers.update({
            _id: _id
        }, {
            $set: {
                userPickingCards:Meteor.userId(),
                isPickingCardsInProgress: true
            }
        })
    },
    finishPickingCards(_id){
        check(Meteor.userId(), String);
        check(_id, String);
        let gtp = GameTeamsPlayers.findOne(_id);

        if (!gtp){
            throw new Meteor.Error("non-existing-player", `player with id ${_id} doesn't exists`);
        }

        //doesn't have to check if only for guests and if user is in game because user was checked on startPickingCards and saved in userPickingCards
        //doesn't have to check state because this is possible only if picking cards started

        if (gtp.userPickingCards !== Meteor.userId()){
            throw new Meteor.Error("access-denied", `Another user picking cards for ${gtp.playerName}`);
        }

        if (gtp.isPickingCardsDone){
            throw new Meteor.Error("access-denied", `${gtp.playerName} already picked cards`);
        }

        let enteredCards = GameCards.find({playerIds: _id}).count();
        let numberOfCardsPerPlayer = Games.findOne(gtp.gameId).preferences.numberOfCardsPerPlayer;

        if (enteredCards !== numberOfCardsPerPlayer){
            throw new Meteor.Error("invalid-state", `You need to pick exactly ${numberOfCardsPerPlayer} cards`);
        }

        GameTeamsPlayers.update({
            _id: _id
        }, {
            $set: {
                isPickingCardsDone:true,
                isPickingCardsInProgress: false
            }
        })
    },
    pickNextPlayer(_id){
        check(Meteor.userId(), String);
        check(_id, String);
        let gtp = GameTeamsPlayers.findOne(_id);

        if (!gtp){
            throw new Meteor.Error("non-existing-player", `player with id ${_id} doesn't exists`);
        }

        let fsm = GameStateMachine(gtp.gameId);
        if (!fsm.is('usersSelectingPlayerOrder')){
            throw new Meteor.Error('wrong-state', 'Game is not in usersSelectingPlayerOrder state');
        }

        if (!isUserInGame(Meteor.userId(), gtp.gameId)){
            throw new Meteor.Error("access-denied", 'You are not participating in this game');
        }

        let gameState = GameState.findOne(gtp.gameId);

        GameTeamsPlayers.update({
            _id: _id
        }, {
            $set: {
                playerOrder: gameState.currentPlayer
            }
        });

        GameTeamsPlayers.update({
            gameId: gtp.gameId,
            teamName: gtp.teamName
        }, {
            $set: {
                teamOrder: gameState.currentTeam
            }
        }, {
            multi: true
        });

        fsm.playerForOrderSelected();

    },
    userReady(_id){
        check(_id, String);

        let game = Games.findOne(_id);

        if (!game) {
            throw new Meteor.Error("Game doesn't exists", `A game with id ${_id} doesn't exists!`);
        }

        let fsm = GameStateMachine(_id);
        if (!fsm.is('waitingUserAcceptPlay')) {
            throw new Meteor.Error('wrong-state', 'Game is not in waitingUserAcceptPlay state');
        }

        if (!isUserInGame(Meteor.userId(), _id)){
            throw new Meteor.Error("access-denied", 'You are not participating in this game');
        }

        GameState.update(_id, {
            $set: { userGuessing: Meteor.userId() }
        })

        fsm.userAcceptedPlay();
    }

});