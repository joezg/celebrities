/* global isDocumentCreator:true*/
// check that the userId specified owns the documents
isDocumentCreator = function(userId, doc) {
    if (!userId)
        userId = Meteor.userId();

    return doc && doc.creatorId === userId;
};

isUserInGame = function(userId, gameId){
    return !!Games.findOne({"users._id": userId, _id: gameId});
};

isUserGuessing = function(userId, gameId){
    let gs = GameState.findOne(gameId);

    if (!gs)
        return false;

    return userId === gs.userGuessing;
}