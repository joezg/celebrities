getUserName = function(user){
    if (user)
        return user.profile && user.profile.name ? user.profile.name : user.username;
    else
        return "";
}
